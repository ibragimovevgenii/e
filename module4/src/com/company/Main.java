package com.company;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
public class Main {
    private static String k[][] = new String[2][2];
    private static final String separator = ";";
    private static String fname = "./src/com/company/Четвертый Модуль.csv";
    public static void main(String[] args) {
        ReadCSV();

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(k[i][j] + "\t");
            }
            System.out.println("");
        }
    }

    public static void ReadCSV() {

        try(BufferedReader br =
                    new BufferedReader(new InputStreamReader(new FileInputStream(fname), "Windows-1251"))) {
            String line = "";
            int i=0;
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] elements = line.split(separator);
                k[i][0]=elements[0];
                k[i][1]=elements[4];
                i++;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

