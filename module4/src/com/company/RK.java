package com.company;
import java.io.*;
import java.util.*;

public class RK{
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Введите строку:");
        String first=in.next();
        System.out.println("Введите подстроку:");
        String second=in.next();
        RabinKarp(first,second);
    }
    static void RabinKarp(String first,String second){
        char[]first1=first.toCharArray();
        char[]second2=second.toCharArray();
        long[]pow=new long[first1.length];
        pow[0]=1;
        for(int i=1;i<first.length();i++)
            pow[i]=(pow[i-1]*2);
        //ПОДСЧЕТ ХЕШ-ФУНКЦИИ ВСЕЙ ПОДСТРОКИ
        long ht=second2[0];
        for (int i = 1; i < second2.length; i++) {
            ht += (pow[i] * second2[i]);}//////////////////
        //ПОДСЧЕТ КАЖДОЙ ХЭШ-ФУНКЦИИ ГЛАВНОЙ СТРОКИ
        long[]hs=new long[first1.length];
        hs[0] = first1[0];
        for (int i = 1; i < first1.length; i++) {
            hs[i] = (hs[i - 1] + pow[i] * first1[i]);
        }
        for (int i = 0; i + second2.length <= first.length(); i++) {
            if (getHashLine(hs, i, i + second2.length - 1) == ht*pow[i]) {
                if (ht*pow[i]==0)
                    break;
                System.out.println("Вхождение после "+i+" элемента");
            }
        }
    }
    static long getHashLine(long[] h, int L, int R) {
        long result = h[R];//////////////////
        if (L > 0) result =(h[R]-h[L - 1]);
        return result;}
    static long Random(long max)
    {
        long a=(long)(Math.random()*(max));
        return a;
    }
}

