package com.company;
import java.util.*;
public class Formation1 {
    public static void main(String[] args) throws InvalidValueOfBalance{
        Scanner in = new Scanner(System.in);
        int dataArray[] = {20, 18, 19, 15, 17, 16, 3, 25, 22, 27, 26, 28, 21, 23};
        Node root = null;
        try{
            System.out.println(root.key);
        }
        catch (NullPointerException e1) {
            System.out.println("NullPointerException");
        }
        try{
            int a = 0, b = 1 / a;
        }
        catch (ArithmeticException e2) {
            System.out.println("ArithmeticException");
        }
        String str1="3f";
        try {
            int int1=Integer.parseInt(str1);
        }
        catch (NumberFormatException e3){
            System.out.println("NumberFormatException");
        }
            Node z = new Node();
        Node first=z;
            z.key = dataArray[0];
            z.parent = null;
            root = insert(z, root);
            try{
                for (int i = 1; i <= dataArray.length; i++) {
                    z = new Node();
                    z.key = dataArray[i];
                    insert(z, root);
                }
            }
            catch (IndexOutOfBoundsException e4){
                System.out.println("IndexOutOfBoundsException");
            }
            try{
                z = new Node();
                z.key = -10;
                insert(z, root);
            }
            catch (IllegalArgumentException e5){
                System.out.println("IllegalArgumentException");
            }
        //ИСКЛЮЧЕНИЕ ДЛЯ ПОИСКА ВЫСОТЫ ВЫСОТЫ
            try {
        System.out.println(h(first));}
            catch (InvalidValueOfBalance ex){
                ex.getMessage();
            }
        System.out.println("Проверка исключения");
        }

    public static Node insert(Node z, Node x){
        if (x == null){
            x = z;
        }
        else if (x.key == z.key)
            throw new IllegalArgumentException();
        else if (z.key < x.key){
            x.left = insert(z, x.left);
            x.left.parent = x;
        } else{
            x.right = insert(z, x.right);
            x.right.parent = x;
        }
        return x;
    }

    public static Node search(Node x, int k){
        if ((x == null) || (k == x.key))
            return x;
        if (k < x.key)
            return search(x.left, k);
        else
            return search(x.right, k);
    }

    public static int h(Node x) throws InvalidValueOfBalance{
        if(x==null)return -1;
        else
            if(Math.abs(h(x.left)-h(x.right))>1)
                throw new InvalidValueOfBalance();
            return Math.max(h(x.left),h(x.right))+1;}

   static class  InvalidValueOfBalance extends Exception {
        @Override
        public String getMessage(){
            return "InvalidBalanceValue";
        }
    }

}