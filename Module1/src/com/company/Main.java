package com.company;
import Jama.*;
import java.util.*;
public class Main {
    static double[]z=new double[4];
    double sum=0;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Выбор уравнения:");
        int vibor = in.nextInt();
        System.out.println("Введите количество уравнений:");
        int n = in.nextInt();
        double[][] x;
        double[] y=new double[n];
        Main A = new Main();
        Button button = new Button(new ButtonClickHandler());
        if (vibor == 1) {
            x= new double[n][4];
            for (int i = 0; i < n; i++) {
                x[i][0] = 1;
            }
            for (int i = 0; i < n; i++) {
                for (int j = 1; j < 4; j++) {
                    System.out.println("Введите x[" + i + "]" + "[" + j + "]");
                    x[i][j] = in.nextInt();
                }
            }
            for (int i = 0; i < n; i++) {
                System.out.println("Введите y[" + i + "]");
                y[i] = in.nextInt();
            }
            button.click(x);
            A.test(x,y);
            A.error(x,y);
        }
        if(vibor==2)
        {
            System.out.println("Введите время");
            int time=in.nextInt();
            x=new double[n][2];
            for (int i = 0; i < n; i++) {
                x[i][0] = 1;
            }
            for (int i = 0; i < n; i++) {
                for (int j = 1; j < 2; j++) {
                    System.out.println("Введите x[" + i + "]" + "[" + j + "]");
                    x[i][j] = time*in.nextInt();
                }
            }
            for (int i = 0; i < n; i++) {
                System.out.println("Введите y[" + i + "]");
                y[i] = in.nextInt();
            }
            A.test(x,y);
            A.error(x,y);

        }
    }
    public void test(double[][]x,double[]y)
    {
        Matrix A1=new Matrix(x);
        A1.transpose();
        Matrix Y=new Matrix(y,y.length);
        System.out.println("Ввод Матрицы X");
        A1.print(1,2);
        System.out.println("Ввод Матрицы Y");
        Y.print(1,2);
        Matrix B1=A1.transpose();
        System.out.println("Ввод Матрицы X транспонированной");
        B1.print(1,2);
        Matrix F1=B1.times(A1);
        System.out.println("Умножение Матрицы X транспонированной на обычную X");
        F1.print(1,2);
        Matrix F4=F1.inverse();
        System.out.println("Обратная матрица полученной");
        F4.print(1,2);
        Matrix F2=F4.times(B1);
        System.out.println("Умножение обратной на транспонированную X");
        F2.print(1,2);
        Matrix F3=F2.times(Y);
        System.out.println("Умножение на вектор Y");
        F3.print(1,2);
        z=F3.getColumnPackedCopy();
        for (int i = 0; i < x[0].length; i++)
            System.out.println("z["+i+"]="+z[i]);
    }
    public void error(double[][]x,double[]y)
    {
        int n=y.length;
        double[]error1=new double[n];
        double[]c=new double[n];
        if(x[0].length==4)
            for(int i=0;i<n;i++)
            {
                c[i]=x[i][0]*z[0]+x[i][1]*z[1]+x[i][2]*z[2]+x[i][3]*z[3];
            }
        if(x[0].length==2)
            for(int i=0;i<n;i++)
            {
                c[i]=x[i][0]*z[0]+x[i][1]*z[1];
            }
        System.out.println("Вывод модели ошибки:");
        for(int i=0;i<n;i++)
        {
            error1[i]=Math.pow(y[i]-c[i],2);
            sum+=error1[i];
        }
        System.out.println(sum);}
}