package com.company;

public class Button {
    Handler handler;
    Button(Handler action)
    {
        this.handler=action;
    }
    public void click(double[][]x)
    {
        handler.execute(x);
    }
}
