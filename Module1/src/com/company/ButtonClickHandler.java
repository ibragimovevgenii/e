package com.company;
import Jama.*;
public class ButtonClickHandler implements Handler {
    static double[] z = new double[4];

    public void execute(double[][] x) {
        for (int i = 0; i < x.length; i++) {
            x[i][1]=Math.cos(Math.toRadians(x[i][1]));
        }
        for (int i = 0; i < x.length; i++) {
            x[i][2]=Math.pow(Math.log(x[i][2]),2);
        }
    }
}

